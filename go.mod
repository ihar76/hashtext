module bitbucket.org/ihar76/hashtext

go 1.14

require (
	github.com/chrisport/go-lang-detector v0.0.0-20201227125515-a4270979d85f
	github.com/kljensen/snowball v0.6.0
)
