package hashtext

import (
	"github.com/chrisport/go-lang-detector/langdet"
	"github.com/chrisport/go-lang-detector/langdet/langdetdef"
	"github.com/kljensen/snowball"
	"strings"
)

func Stemm(word *string) {

	wordTemp := strings.ToLower(*word)
	detector := langdet.NewDetector()
	detector.AddLanguageComparators(langdetdef.RUSSIAN, langdetdef.ENGLISH)

	result := detector.GetLanguages(wordTemp)
	stemmed, err := snowball.Stem(wordTemp, result[0].Name, true)
	if err == nil && stemmed != "" {
		*word = stemmed
	}

}

func Hash(intext string) int {

	//Stemm(&intext)

	hash := uint(0)
	// if you care this can be done much faster with unsafe
	// using fixed char* reinterpreted as a byte*
	for _, byteSng := range intext {
		hash += uint(byteSng)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}
	// final avalanche
	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)
	// helpfully we only want positive integer < MUST_BE_LESS_THAN
	// so simple truncate cast is ok if not perfect
	return int(hash)

}
